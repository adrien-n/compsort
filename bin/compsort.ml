open Lib.Util

let f ~alg ~visualization_pre ~visualization_post ~db_path ~paths =
  let load () =
    let domain_count = Domain.recommended_domain_count () in
    Lib.Hc.f ~domain_count ~db_path ~paths
  in
  let prepare ~files ~overlaps =
    let file_indexes = Lib.Bettercomp.noop ~files ~overlaps in
    visualization_pre |> Option.iter (fun output ->
      Lib.Plot.png ~files ~overlaps ~file_indexes ~output;
      Lib.Plot.plotly ~files ~overlaps ~file_indexes ~output
    )
  in
  let exec ~files ~overlaps ~alg =
    let file_indexes = alg ~files ~overlaps in
    visualization_post |> Option.iter (fun output ->
      Lib.Plot.png ~files ~overlaps ~file_indexes ~output;
      Lib.Plot.plotly ~files ~overlaps ~file_indexes ~output
    );
    file_indexes
  in
  let finish ~files ~overlaps ~file_indexes =
    for i = 0 to files.count - 2 do
      Printf.printf "%s %s %d\n" files.paths.(file_indexes.(i)) files.paths.(file_indexes.(i+1)) overlaps.(file_indexes.(i)).(file_indexes.(i+1))
    done;
    print_endline files.paths.(files.count - 1)
  in
  let files, overlaps = load () in
  prepare ~files ~overlaps;
  let file_indexes = exec ~files ~overlaps ~alg in
  finish ~files ~overlaps ~file_indexes

module CLI = struct
  open Cmdliner

  let algs =
    Lib.Bettercomp.([
      "cluster", cluster;
      "shortest", shortest;
      "noop", noop;
      "buckets", buckets;
    ])

  let cluster_algorithm =
    let doc = "Clustering algorithm used for sorting" in
    Arg.(value & opt (enum algs) Lib.Bettercomp.buckets & info [ "cluster" ] ~docv:"ALGORITHM" ~doc)

  let visualization_pre =
    let doc = "Write pre-sort visualization as png to FILE" in
    Arg.(value & opt (some string) None & info [ "pre-sort-visualization" ] ~docv:"FILE" ~doc)

  let visualization_post =
    let doc = "Write post-sort visualization as png to FILE" in
    Arg.(value & opt (some string) None & info [ "post-sort-visualization" ] ~docv:"FILE" ~doc)

  let db =
    let doc = "Path to the results database" in
    Arg.(value & opt string "database" & info [ "database" ] ~docv:"FILE" ~doc)

  let paths =
    let doc = "Files to sort" in
    Arg.(non_empty & pos_right 0 string [] & info [] ~docv:"FILES" ~doc)

  let compsort =
    let invoke alg visualization_pre visualization_post db_path paths =
      f ~alg ~visualization_pre ~visualization_post ~db_path ~paths:(Array.of_list paths)
    in
    Term.(const
      invoke
      $ cluster_algorithm
      $ visualization_pre
      $ visualization_post
      $ db
      $ paths
    )

  let cmd =
    let doc = "Sort files according to similarity for improved compression" in
    let man = Manpage.([
      `S s_authors;
      `P "Adrien Nader <adrien@notk.org>";
    ])
    in
    let info = Cmd.info "compsort" ~doc ~man in
    Cmd.v info compsort
end

let () =
  exit (Cmdliner.Cmd.eval CLI.cmd)
