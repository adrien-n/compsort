open Logs
open Util

let png ~files ~overlaps ~file_indexes ~output =
  let open Image in
  let scale = 65535 in
  info (fun m -> m "Generate adjacency graphs");
  let file_count = Array.length file_indexes in
  let img = create_grey file_count file_count in
  info (fun m -> m "Add adjacency data");
  for x = 0 to file_count - 1 do
    for y = 0 to file_count - 1 do
      let t =
        float overlaps.(file_indexes.(x)).(file_indexes.(y))
        /.
          (max
            (float (
               files.sizes.(file_indexes.(x))
               + files.sizes.(file_indexes.(y))
               - overlaps.(file_indexes.(x)).(file_indexes.(y))))
            1.)
      in
      write_grey img x y (truncate (float scale *. t))
    done;
  done;
  let tmp = output ^ ".tmp" in
  let writer = ImageUtil_unix.chunk_writer_of_path tmp in
  ImagePNG.write_png writer img;
  Unix.rename tmp output


module PlotlyFork = struct
  (* This module is almost a copy of plotly-ocaml's python.ml file except it
     has an added ?extra_attributes parameter for customization.
     Original license below.

     MIT License
     Copyright (c) 2023 DaiLambda, Inc. <contact@dailambda.jp>

     Permission is hereby granted, free of charge, to any person obtaining a
     copy of this software and associated documentation files (the "Software"),
     to deal in the Software without restriction, including without limitation
     the rights to use, copy, modify, merge, publish, distribute, sublicense,
     and/or sell copies of the Software, and to permit persons to whom the
     Software is furnished to do so, subject to the following conditions:

     The above copyright notice and this permission notice shall be included
     in all copies or substantial portions of the Software.

     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
     THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
     DEALINGS IN THE SOFTWARE.
   *)

  open Plotly
  open Figure

  let extra_attributes ~text ~z =
    let marker_attributes = [
      "colorscale", Py.String.of_string "Viridis";
      "color", Py.List.of_array_map Py.Float.of_float z;
      "size", Py.Int.of_int 1;
    ]
    in
    [
      "marker", Py.Dict.of_bindings_string marker_attributes;
      "text", Py.List.of_array_map Py.String.of_string text;
    ]

  let rec of_value v =
    let open Value in
    match v with
    | Value (Float, f) -> Py.Float.of_float f
    | Value (String, s) -> Py.String.of_string s
    | Value (Array ty, vs) ->
        Py.List.of_array @@ Array.map (fun v -> of_value (Value (ty, v))) vs

  let of_attribute (s, v : Attribute.t) = (s, of_value v)

  let of_attributes = List.map of_attribute

  let init () =
    if not @@ Py.is_initialized () then Py.initialize ()

  let go =
    init ();
    Py.Import.import_module "plotly.graph_objects"

  let of_graph ?(extra_attributes = []) Graph.{type_; data} =
    let c =
      match type_ with
      | "scatter" -> "Scatter"
      | "scatter3d" -> "Scatter3d"
      | "bar" -> "Bar"
      | "pie" -> "Pie"
      | _ -> assert false
    in
    Py.Module.get_function_with_keywords go c [||]
      (of_attributes (data :> Attribute.t list) @ extra_attributes)

  let of_layout layout =
    let layout = (layout : Layout.t :> Attribute.t list) in
    Py.Dict.of_bindings_map
      Py.String.of_string
      of_value
      layout

  let of_figure ?extra_attributes fig =
    let data = Py.List.of_list_map (of_graph ?extra_attributes) fig.Figure.graphs in
    let layout = of_layout fig.layout in
    Py.Module.get_function_with_keywords go "Figure" [||]
      ["data", data;
       "layout", layout]

  let show ?renderer figure =
    match Py.Object.get_attr_string figure "show" with
    | None -> failwith "no show"
    | Some show ->
        let show = Py.Callable.to_function_with_keywords show [||] in
        match renderer with
        | None -> ignore @@ show []
        | Some n -> ignore @@ show ["renderer", Py.String.of_string n]

end

let plotly ~files ~overlaps ~file_indexes ~output =
  ignore output;
  let file_count = Array.length file_indexes in
  let stack = Stack.create () in
  for x = 0 to file_count - 1 do
    for y = 0 to file_count - 1 do
      let t =
        float overlaps.(file_indexes.(x)).(file_indexes.(y))
        /.
          (max
            (float (
               files.sizes.(file_indexes.(x))
               + files.sizes.(file_indexes.(y))
               - overlaps.(file_indexes.(x)).(file_indexes.(y))))
            1.)
      in
      if t > 0.01 && x <> y then
        Stack.push (float x, float y, min 1.2 t) stack
    done;
  done;
  let array = stack |> Stack.to_seq |> Array.of_seq in
  let text = Array.map (fun (x, y, _z) ->
    let x = file_indexes.(truncate x) in
    let y = file_indexes.(truncate y) in
    Printf.sprintf "%s[%d] | %s[%d]" files.paths.(x) files.sizes.(x) files.paths.(y) files.sizes.(y)) array
  in
  let z = Array.map (fun (_, _, z) -> z) array in
  let figure =
    let open Plotly in
    Figure.figure [
      Graph.scatter3d [
        Data.xyz array;
        Data.mode "markers";
      ]
    ]
    [
      Layout.title output
    ]
  in
  let open PlotlyFork in
  let extra_attributes = extra_attributes ~text ~z in
  show (of_figure ~extra_attributes figure)
