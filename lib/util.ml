module Logging = struct
  let t0 = Unix.gettimeofday ()

  let reporter ppf =
    let report _src level ~over k msgf =
      let k _ = over (); k () in
      msgf @@ fun ?header ?tags fmt ->
          Format.kfprintf k ppf ("%a[%04.6f] @[" ^^ fmt ^^ "@]@.")
            Logs.pp_header (level, header) (Unix.gettimeofday () -. t0)
    in
    { Logs.report = report }

  let () =
    Logs.set_reporter (reporter Format.err_formatter);
    Logs.set_level (Some Logs.Info);
end

module DB = struct
  open Sqlite3

  let db path =
    let db = db_open path in
    busy_timeout db (1000 * Random.int 1000);
    List.iter (fun s -> Rc.check (exec db s)) [
      "create table if not exists files (hash integer not null unique, size integer);";
      "create table if not exists overlaps_hc (id1 integer not null, id2 integer not null, overlap integer);";
      "create index if not exists files_index on files(hash);";
      "create index if not exists overlaps_index on overlaps_hc(id1, id2);";
      "pragma busy_timeout = 10000";
      "pragma optimize = 0x10002;";
      "pragma journal_mode = WAL;";
      "pragma synchronous = normal;";
      "pragma temp_store = memory;";
      "pragma mmap_size = 30000000000;";
      "pragma page_size = 32768;";
    ];
    db

  let init_files ~db ~hashes ~sizes =
    let select = prepare db "select (rowid) from files where hash = ? order by rowid asc limit 1" in
    let insert = prepare db "insert or replace into files (hash, size) values (?, ?)" in
    let file_count = Array.length hashes in
    let file_of_sql_rowid = Hashtbl.create file_count in
    let sql_rowid_of_file = Array.make file_count 0 in
    for x = 0 to file_count - 1 do
      Rc.check @@ bind_values select [
        INT (Int64.of_int32 hashes.(x));
      ];
      (if step select = Rc.ROW then (
        let rowid = column_int select 0 in
        sql_rowid_of_file.(x) <- rowid;
        Hashtbl.add file_of_sql_rowid rowid x;
      )
      else (
        Rc.check @@ bind_values insert [
          INT (Int64.of_int32 hashes.(x));
          INT (Int64.of_int sizes.(x));
        ];
        Rc.check @@ step insert;
        let rowid = Int64.to_int (Sqlite3.last_insert_rowid db) in
        sql_rowid_of_file.(x) <- rowid;
        Hashtbl.add file_of_sql_rowid rowid x;
        Rc.check @@ reset insert
      )
      );
      Rc.check @@ reset select
    done;
    file_of_sql_rowid, sql_rowid_of_file

  let fetch_overlap_stmt ~db =
    prepare db "select (overlap) from overlaps_hc where id1 = ? and id2 = ?;"

  let fetch_overlap ~stmt ~id1 ~id2 =
    Rc.check @@ (bind_values stmt [
      INT (Int64.of_int id1);
      INT (Int64.of_int id2);
      ]);
    if step stmt = Rc.ROW then (
      let res = column_int stmt 0 in
      Rc.check @@ reset stmt;
      res
    )
    else (
      Rc.check @@ reset stmt;
      -1
    )

  let store_overlap_stmt ~db =
    prepare db "insert or replace into overlaps_hc (id1, id2, overlap) values (?, ?, ?);"

  let store_overlap ~stmt ~sql_rowid_of_file x y v =
    Rc.check @@ (bind_values stmt [
      INT (Int64.of_int sql_rowid_of_file.(x));
      INT (Int64.of_int sql_rowid_of_file.(y));
      INT (Int64.of_int v);
      ]);
    Rc.check @@ step stmt;
    Rc.check @@ reset stmt

  let begin_transaction ~immediate db =
    Rc.check @@ exec db
      (if immediate then "begin immediate transaction;" else "begin transaction;")

  let end_transaction db =
    Rc.check @@ exec db "end transaction;"
end

let map_file f =
  let fd = Unix.(openfile f [ O_RDONLY; O_CLOEXEC ] 0o644) in
  let map = Unix.map_file fd ~pos:0L Bigarray.char Bigarray.c_layout false [| -1 |] in
  let dims = Bigarray.Genarray.dims map in
  fd, Bigarray.reshape_1 map dims.(0)

type files = {
  paths : string array;
  count : int;
  sizes : int array;
  hashes : int32 array;
  of_sql_rowid : (int, int) Hashtbl.t; (* from rowid to file id *)
  to_sql_rowid : int array;
}

module Files = struct
  type t = files

  let hashes ~paths =
    Array.map (fun path ->
      let fd, ba = map_file path in
      let h = Murmur3.hash32 (Bigstringaf.to_string ba) in
      Unix.close fd;
      h
    ) paths

  let sizes ~count ~paths =
    let sizes = Array.make count 0 in
    for x = 0 to count - 1 do
      let size = Unix.((stat paths.(x)).st_size) in
      sizes.(x) <- size;
    done;
    sizes

  let make ~db ~paths =
    let count = Array.length paths in
    let hashes = hashes ~paths in
    let sizes = sizes ~paths ~count in
    let of_sql_rowid, to_sql_rowid = DB.init_files ~db ~hashes ~sizes in
    {
      paths;
      count;
      sizes;
      hashes;
      of_sql_rowid;
      to_sql_rowid;
    }

end

let () = Random.self_init ()
