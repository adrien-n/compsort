open Logs
open Util

module BA1 = Bigarray.Array1

module BA_uint8 = struct
  include Bigarray.Array1

  type ba = (int, Bigarray.int8_unsigned_elt, Bigarray.c_layout) t

  let[@inline always] get (ba : ba) i =
    unsafe_get ba i

  let[@inline always] set (ba : ba) i v =
    unsafe_set ba i v

  let[@inline always] fill (ba : ba) v =
    fill ba v
end

module BA_int32 = struct
  include Bigarray.Array1

  type ba = (int32, Bigarray.int32_elt, Bigarray.c_layout) t

  let[@inline always] get (ba : ba) i =
    Int32.to_int (unsafe_get ba i)

  let[@inline always] set (ba : ba) i v =
    unsafe_set ba i (Int32.of_int v)

  let[@inline always] fill (ba : ba) v =
    fill ba v
end

let check_n f1 f1i f1l f2 f2i f2l check_length =
  let l_max = min (f1l - f1i) (f2l - f2i) in
  let l = ref 0 in
  let step = ref check_length in
  (* Printf.eprintf "%d %d %d %d\n" f1l f2l f1i f2i; *)
  while (!step >= 1) && !l < l_max do
    (* prerr_int !step; *)
    if Bigstringaf.unsafe_memcmp f1 (f1i + !l) f2 (f2i + !l) (min !step (l_max - !l)) = 0 then (
      l := !l + !step;
      step := !step * 2;
      (* prerr_string "+ "; *)
    )
    else (
      step := !step / 2;
      (* prerr_string "- "; *)
    )
  done;
  (* Printf.eprintf "=> %d\n" !l; *)
  !l

(* Hash check_length bytes of ba starting at off *)
let h ba ~off ~check_length =
  Int32.to_int (Int32.shift_right_logical (Murmur3.hash32 (Bigstringaf.substring ba ~off ~len:check_length)) 8)

let check_length =
  let check_length = 8 in
  max 3 check_length

(* For every provided file, for every position in the file, hash check_length
   bytes of the file and record that  *)
let rhashes ~domain_count ~paths =
  let file_count = Array.length paths in
  let pool = Domainslib.Task.setup_pool ~num_domains:(domain_count - 1) () in
  let rhashes = Array.make file_count [| |] in
  info (fun m -> m "Hash files");
  let _ = Domainslib.Task.run pool (fun _ ->
    Domainslib.Task.parallel_for pool ~start:0 ~finish:(file_count - 1) ~body:(fun x ->
      let path = paths.(x) in
      let fd, f = map_file path in
      let l = Bigarray.Array1.dim f in
      let a = Array.make l (-1) in
      for i = 0 to l - check_length do
        Array.unsafe_set a i (h f ~off:i ~check_length)
      done;
      Unix.close fd;
      rhashes.(x) <- a
    )
  )
  in
  Domainslib.Task.teardown_pool pool;
  rhashes

(* Fill [matcher] based on [file]:
   - for every position [i] in [file]
     - let [k] be the hash of (check_length] bytes starting at that position
     - [matcher.(k) <- i]
   The loop is unrolled for performance. *)
let matcher_prepare ~matcher ~file ~l =
  BA_int32.fill matcher (-1l);
  for i = 0 to (l - check_length) / 4 - 3 do
    (* no gain from unrolling further *)
    let k0 = h file ~off:i ~check_length in
    let k1 = h file ~off:(i+1) ~check_length in
    let k2 = h file ~off:(i+2) ~check_length in
    let k3 = h file ~off:(i+3) ~check_length in
    BA_int32.set matcher k0 i;
    BA_int32.set matcher k1 (i+1);
    BA_int32.set matcher k2 (i+2);
    BA_int32.set matcher k3 (i+3);
  done

type per_thread = {
  id : int;
  matcher : BA_int32.ba;
  matcher_mask : BA_uint8.ba;
  matcher_hits : int array;
  db : Sqlite3.db;
  store_overlap : sql_rowid_of_file:int array -> int -> int -> int -> unit;
  fetch_overlap : id1:int -> id2:int -> int
}

let bar ~files ~rhashes ~per_thread ~hits ~f1 ~f1l ~y =
  let { matcher; matcher_mask; matcher_hits; _ } = per_thread in

  (* If we were able to record all hits in matcher in the matcher_hits
     array, set the corresponding indices back to 0 individually.
     Otherwise, fill all the array. *)
  (if !hits < Array.length matcher_hits then (
    for i = 0 to !hits - 1 do
      BA_uint8.set matcher_mask (Array.unsafe_get matcher_hits i) 0;
    done;
  )
  else (
    BA_uint8.fill matcher_mask 0;
  )
  );
  hits := 0;

  let c = ref 0 in

  (* Various initilizations before starting the actual processing *)
  let f2_path = files.paths.(y) in
  let f2fd, f2 = map_file f2_path in
  let f2l = BA1.dim f2 in
  let j = ref 0 in
  let hashes_y = rhashes.(y) in

  while !j < f2l - check_length do
    (* For every byte of f2l, get its hash at that location. *)
    let k = Array.unsafe_get hashes_y !j in
    (* Don't go further if the hash has already lead to a match. *)
    if BA_uint8.get matcher_mask k = 0 then (
      (* Lookup in files.paths.(x) where the same hash exists. *)
      let i = BA_int32.get matcher k in
      (* If the hash also exists in files.paths.(x) ... *)
      if i >= 0 then (
        (* Find how many bytes match between the two files at their
           respective locations. *)
        let len = check_n f1 i f1l f2 !j f2l check_length in
        (* Don't count short matches which are not very relevant in practice *)
        if len >= check_length then (
          (* Record which hash lead to the match for quick undo *)
          (if !hits < Array.length matcher_hits then (
            Array.unsafe_set matcher_hits !hits k;
            incr hits;
          ));
          (* Mask the hash which lead here *)
          BA_uint8.set matcher_mask k 1;
          (* Increase the count of re-used bytes *)
          c := !c + len;
          (* Printf.eprintf "match: %d@%d [%d]\n%!" len !j !c; *)
          (* Move further in the files, skipping the data that matched. *)
        );
        j := !j + len + 1
      )
      else (
        incr j;
      )
    )
    else (
      (* Might not be wise to skip check_length bytes at once *)
      j := !j + check_length;
      (* incr j; *)
    )
  done;
  Unix.close f2fd;
  !c

let make_results ~per_thread ~files =
  let results = Array.make_matrix files.count files.count (-1) in
  for i = 0 to files.count - 1 do
    for j = 0 to files.count - 1 do
      let id1 = files.to_sql_rowid.(i) in
      let id2 = files.to_sql_rowid.(j) in
      let overlap = per_thread.fetch_overlap ~id1 ~id2 in
      (* There can be duplicate files in the input set and we need to make
         sure none gets shadowed by others so use find_all and iter *)
      let xl = Hashtbl.find_all files.of_sql_rowid id1 in
      let yl = Hashtbl.find_all files.of_sql_rowid id2 in
      List.iter (fun x ->
        List.iter (fun y ->
          results.(x).(y) <- overlap
        ) yl
      ) xl
    done;
  done;
  results

let foo ~files ~rhashes ~per_thread ~results x =
  (* results.(x).(y) is the count of repeated bytes across files.(x) and files.(y) *)
  let results = results.(x) in

  let { matcher; matcher_mask; db; store_overlap; _ } = per_thread in

  let f1_path = files.paths.(x) in
  let f1fd, f1 = map_file f1_path in
  let f1l = BA1.dim f1 in

  (* Initialize matcher *)
  matcher_prepare ~matcher ~file:f1 ~l:f1l;
  BA_uint8.fill matcher_mask 0;

  let hits = ref 0 in

  (* Test every file combination. *)
  for y = 0 to files.count - 1 do
    (if results.(y) < 0 then
      let v =
        if files.hashes.(x) = files.hashes.(y) then
          f1l
        else
          bar ~files ~rhashes ~per_thread ~hits ~f1 ~f1l ~y
      in
      results.(y) <- v
    );
  done;

  DB.begin_transaction ~immediate:true db;
  for y = 0 to files.count - 1 do
    if results.(y) >= 0 then
      store_overlap ~sql_rowid_of_file:files.to_sql_rowid x y results.(y)
  done;
  DB.end_transaction db;

  Unix.close f1fd

let resource_pool ~db_path ~domain_count =
  let chan = Domainslib.Chan.make_bounded domain_count in
  for id = 0 to domain_count - 1 do
    (* matcher maps a hash to an index; the array is allocated once and re-used
       for all files *)
    let matcher = Bigarray.(Array1.create Int32 c_layout (1 lsl 24)) in
    (* matcher should only lead to a match once per hash (at least in the current
       implementation); the obvious way to do it is to set matcher.(hash) to 0
       once a match has been found but this is incurs a very large performance
       penalty; it's much faster to have a separate array that sole purpose is to
       indicate if hash has already lead to a match.
       The array is Int8_unsigned as it's more compact; a bitfield could be even
       more compact but it turns out it's slower, probably due to the need to
       first fetch the value instead of setting it directly. *)
    let matcher_mask = Bigarray.(Array1.create Int8_unsigned c_layout (1 lsl 24)) in
    (* Since matcher_mask is re-used across files, it must be reset between
       reuses. The obvious way is to fill it with 0 but this is slow. Instead we
       store which indices have been modified in an array of up to 8192 elements
       and specifically reset only these indices.
       NB: 8192 has been chosen because tests indicated 768 improved performance
       as much as possible on a smaller testcase and there was no slowdown from
       larger arrays. *)
    let matcher_hits = Array.make 8192 0 in
    let db = DB.db db_path in
    Domainslib.Chan.send chan {
      id;
      db;
      store_overlap = DB.store_overlap ~stmt:(DB.store_overlap_stmt ~db);
      fetch_overlap = DB.fetch_overlap ~stmt:(DB.fetch_overlap_stmt ~db);
      matcher;
      matcher_mask;
      matcher_hits
    }
  done;
  chan

let run ~domain_count ~resource_pool ~paths ~rhashes =
  let pool = Domainslib.Task.setup_pool ~num_domains:(domain_count - 1) () in
  let module T = Domainslib.Task in
  let work_done = ref 0 in
  info (fun m -> m "Open database");
  let { db; _ } as per_thread = Domainslib.Chan.recv resource_pool in
  info (fun m -> m "Load input files");
  let files = Files.make ~db ~paths in
  info (fun m -> m "Load existing results");
  let results = make_results ~per_thread ~files in
  Domainslib.Chan.send resource_pool per_thread;
  info (fun m -> m "Compute overlaps");
  let _ = Domainslib.Task.run pool (fun _ ->
    (* Iterate over every file *)
    Domainslib.Task.parallel_for pool ~start:0 ~finish:(files.count - 1) ~body:(fun x ->
      let per_thread = Domainslib.Chan.recv resource_pool in
      (if Array.exists ((>) 0) results.(x) then (
        foo ~files ~rhashes ~per_thread ~results x
      ));
      Domainslib.Chan.send resource_pool per_thread;
      incr work_done;
      Printf.eprintf "% 2d%%%!" ((100 * !work_done) / files.count)
    )
  ) in
  prerr_newline ();
  info (fun m -> m "Done");
  Domainslib.Task.teardown_pool pool;
  files, results

let f ~domain_count ~db_path ~paths =
  let resource_pool = resource_pool ~domain_count ~db_path in
  let rhashes = rhashes ~domain_count ~paths in
  run ~domain_count ~resource_pool ~paths ~rhashes;
