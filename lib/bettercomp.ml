open Logs
open Util

module type Input = sig
  val files : Files.t
  val overlaps : int array array
end

module X (Input : Input) = struct
  open Input

  let () = info (fun m -> m "Cluster files")

  module Cluster = struct
    type elt = int

    type t = {
      indexes: elt array;
      (* repetitions : int array; *)
      travelled : int array;
      size : int array;
    }

    let singleton i = {
      indexes = [| i |];
      (* repetitions = [| 0 |]; *)
      travelled = [| 0 |];
      size = [| files.sizes.(i) |];
    }

    let dict = 32 * 1024 * 1024

    let rec predict arr file_overlaps e travelled i fs =
      if (travelled < dict) && (i >= 0) && (e < fs) then
        let e = e + file_overlaps.(arr.(i)) in
        let travelled = travelled + files.sizes.(arr.(i)) in
        predict arr file_overlaps e travelled (i - 1) fs
      else
        min e fs, travelled

    let dist a b =
      (* NB: [a] will be located after [b] *)
      (* let a_alone_o = ref 0 in *)
      let a_combined_o = ref 0. in
      let a_end = Array.length a.indexes - 1 in
      let b_end = Array.length b.indexes - 1 in
      for i = 0 to a_end do
        (* a_alone_o := !a_alone_o + a.repetitions.(i); *)
        if a.travelled.(i) < dict then
          let r, t = predict b.indexes overlaps.(a.indexes.(i)) 0 a.travelled.(i) b_end files.sizes.(a.indexes.(i)) in
          a_combined_o := !a_combined_o +. float r /. float (files.sizes.(a.indexes.(i)) + t)
      done;
      -. !a_combined_o

    let join a b =
      let indexes = Array.concat [ a.indexes; b.indexes ] in
      (* let repetitions = Array.make (Array.length indexes) 0 in *)
      let travelled = Array.make (Array.length indexes) 0 in
      let size = Array.make (Array.length indexes) 0 in
      (* Array.blit a.repetitions 0 repetitions 0 (Array.length a.indexes); *)
      Array.blit a.travelled 0 travelled 0 (Array.length a.indexes);
      Array.blit a.size 0 size 0 (Array.length a.indexes);
      let a_size = size.(Array.length a.travelled - 1) in
      for i = Array.length a.travelled to Array.length travelled - 1 do
        let indexes_i = indexes.(i) in
        let _r, t = predict indexes overlaps.(indexes_i) 0 0 (i - 1) files.sizes.(indexes_i) in
        (* repetitions.(i) <- r; *)
        travelled.(i) <- t;
        size.(i) <- size.(i) + a_size;
      done;
      { indexes; (* repetitions; *) travelled; size }
  end

  (* module ClusterMin = struct
    type elt = int

    type t = elt array

    let singleton x = [| x |]

    let dist a b =
      let d = ref 1. in
      for i = 0 to Array.length a - 1 do
        let distances = distances.(a.(i)) in
        for j = 0 to Array.length b - 1 do
          d := !d +. distances.(b.(j));
        done;
      done;
      !d /. float (Array.length a + Array.length b)

    let join a b =
      Array.concat [ a; b ]
  end *)

  module Agg = struct
    include Clustering.Agglomerative.Make(Cluster)

    let rec to_list acc = function
      | { tree = Node (left, right); _ } ->
          let acc = to_list acc left in
          to_list acc right
      | { tree = Leaf; set; _ } ->
          set.indexes.(0) :: acc
  end

  let run () =
    let rec ints_until n acc = function
      | m when m > n -> acc
      | m -> ints_until n (m :: acc) (m + 1)
    in
    let c = Agg.cluster (ints_until (files.count - 1) [] 0) in
    let l = Agg.to_list [] c in
    info (fun m -> m "Finished");
    Array.of_list l
end

module type Run = sig
  val run : unit -> int array
end

let debug = false

let cluster ~files ~overlaps =
  let input = (module struct
    let files = files
    let overlaps = overlaps
  end : Input) in
  let module Run = (val (module X (val input : Input) : Run) : Run) in
  Run.run ()

let shortest ~files ~overlaps =
  let overlaps = Array.copy overlaps in
  for x = 0 to files.count - 1 do
    overlaps.(x).(x) <- min_int;
  done;
  (* print_endline files.paths.(files.count - 1);
  for x' = 0 to files.count - 1 do
    overlaps.(files.count - 1).(x') <- min_int;
  done; *)
  Array.fold_left (fun (x, l) _ ->
    let best = ref neg_infinity in
    let best_index = ref max_int in
    for y = 0 to files.count - 1 do
      let t = float overlaps.(y).(x) /. (max (float (files.sizes.(x) + files.sizes.(y) - overlaps.(y).(x))) 1.) in
      (if t > !best then (
        best := t;
        best_index := y;
      ));
    done;
    assert (!best_index >= 0);
    (if debug then
       Printf.printf "% f %120s [% 4d] %120s [% 4d] (%d)\n" !best files.paths.(x) x files.paths.(!best_index) !best_index overlaps.(!best_index).(x));
    for x' = 0 to files.count - 1 do
      overlaps.(!best_index).(x') <- min_int;
    done;
    !best_index, (!best_index :: l)
  ) (0, [ 0 ]) files.paths
  |> snd
  |> List.rev
  |> Array.of_list

let noop ~files ~overlaps =
  ignore overlaps;
  Array.init files.count (fun i -> i)

let buckets ~files ~overlaps =
  let l = files.paths |> Array.mapi (fun i _ -> [ i ]) |> Array.to_list in
  let likeliness x y =
    float overlaps.(x).(y) /. (float (files.sizes.(x) + files.sizes.(y)))
  in
  let shall_cluster ~threshold a b =
    let a = Array.of_list a in
    let b = Array.of_list b in
    let d = ref 0 in
    for x = 0 to Array.length a - 1 do
      for y = 0 to Array.length b - 1 do
        let likeliness = likeliness a.(x) b.(y) in
        (* Printf.eprintf "likeliness: %f\n" likeliness; *)
        if likeliness >= threshold then
          incr d
      done
    done;
    float !d >= threshold *. (float ((Array.length a) * (Array.length b)))
  in
  let rec f ~threshold (buckets : int list list) = function
    | [] ->
      List.rev buckets
    | (target : int list) :: rest ->
      let current, rest = ListLabels.partition rest ~f:(fun bucket ->
          shall_cluster ~threshold target bucket)
      in
      f ~threshold ((List.concat (target :: current)) :: buckets) rest
  in
  let threshold = ref 2. in
  let thresholds = ref [] in
  while !threshold >= 0.001 do
    thresholds := !threshold :: !thresholds;
    threshold := !threshold *. 0.95
  done;
  (* let thresholds = List.rev_map ((-.) 2.) !thresholds in *)
  let thresholds = List.rev !thresholds in
  List.fold_left (fun buckets threshold -> f ~threshold [] buckets) l thresholds
  |> List.concat
  |> Array.of_list
